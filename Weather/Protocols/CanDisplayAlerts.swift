//
//  CanDisplayAlerts.swift
//  Weather
//
//  Created by Temirlan Merekeyev on 10/28/19.
//  Copyright © 2019 Merekeyev. All rights reserved.
//

import UIKit

protocol CanDisplayAlerts { }

extension CanDisplayAlerts where Self: UIViewController {
    
    func showOkAlert(title: String? = nil, message: String, completion: (() -> Void)? = nil) {
        showInfoAlert(title: title, message: message, positiveCompletion: completion)
    }
    
    func showOkCancelAlert(title: String? = nil,
                           message: String,
                           okCompletion: (() -> Void)? = nil,
                           cancelCompletion: (() -> Void)? = nil) {
        showInfoAlert(title: title,
                    message: message,
                    cancel: true,
                    positiveCompletion: okCompletion,
                    negativeCompletion: cancelCompletion)
    }
    
    func showYesCancelAlert(title: String? = nil, message: String, completion: (() -> Void)? = nil) {
        showInfoAlert(title: title,
                    message: message,
                    positiveButtonTitle: "Yes",
                    cancel: true,
                    positiveCompletion: completion)
    }
    
    func showErrorAlert(title: String? = nil, message: String, completion: (() -> Void)? = nil) {
        showInfoAlert(title: title, message: message, positiveCompletion: completion)
    }
    
    private func showInfoAlert(title: String?,
                               message: String?,
                               positiveButtonTitle: String? = "OK",
                               negativeButtonTitle: String? = "Cancel",
                               cancel: Bool = false,
                               positiveCompletion: (() -> Void)? = nil,
                               negativeCompletion: (() -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: positiveButtonTitle, style: .default, handler: { _ in
            positiveCompletion?()
        }))
        
        if cancel {
            alert.addAction(UIAlertAction(title: negativeButtonTitle, style: .cancel, handler: { _ in
                negativeCompletion?()
            }))
        }
        
        present(alert, animated: true, completion: nil)
    }
}
