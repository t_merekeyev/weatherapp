//
//  CityDetailTableViewCell.swift
//  Weather
//
//  Created by Temirlan Merekeyev on 10/28/19.
//  Copyright © 2019 Merekeyev. All rights reserved.
//

import UIKit

class CityDetailTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    func configure(with title: String, and description: String) {
        titleLabel.text = title
        descriptionLabel.text = description
    }
}
