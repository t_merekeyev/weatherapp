//
//  CityDetailsViewController.swift
//  Weather
//
//  Created by Temirlan Merekeyev on 10/28/19.
//  Copyright © 2019 Merekeyev. All rights reserved.
//

import UIKit

class CityDetailsViewController: UIViewController, CanDisplayAlerts {

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    private let city: City
    private let repository: CityRepository
    private var weather: Weather?
    
    init(city: City, repository: CityRepository) {
        self.city = city
        self.repository = repository
        super.init(nibName: CityDetailsViewController.identifier, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        getWeather()
    }

    private func setupView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CityDetailTableViewCell.nib, forCellReuseIdentifier: CityDetailTableViewCell.identifier)
        
        navigationItem.title = city.name
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    private func getWeather() {
        activityIndicator.startAnimating()
        tableView.isHidden = true
        repository.getWeather(by: city.name, success: { [weak self] weather in
            guard let self = self else { return }
            self.weather = weather
            guard let refreshControl = self.tableView.refreshControl else { return }
            refreshControl.endRefreshing()
            self.activityIndicator.stopAnimating()
            self.tableView.isHidden = false
            self.tableView.reloadData()
        }, failure: { message in
            self.activityIndicator.stopAnimating()
            self.showErrorAlert(message: message)
        })
    }
    
    @objc
    private func refresh() {
        guard let refreshControl = self.tableView.refreshControl else { return }
        refreshControl.beginRefreshing()
        
        activityIndicator.startAnimating()
        repository.getWeather(by: city.name, success: { [weak self] weather in
            guard let self = self else { return }
            self.weather = weather
            guard let refreshControl = self.tableView.refreshControl else { return }
            refreshControl.endRefreshing()
            self.activityIndicator.stopAnimating()
            self.tableView.reloadData()
        }, failure: { message in
            self.activityIndicator.stopAnimating()
            self.showErrorAlert(message: message)
        })
    }
}

extension CityDetailsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0, 3:
            return 1
        case 1:
            return 5
        default:
            return 2
        }
    }
    
    // Have not enough time to do it's more abstractable. I guess it is ok for example
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let weather = weather else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: CityDetailTableViewCell.identifier, for: indexPath) as! CityDetailTableViewCell
        switch indexPath.section {
        case 0:
            cell.configure(with: "Weather", and: weather.status)
        case 1:
            switch indexPath.row {
            case 0:
                cell.configure(with: "Average Temperature", and: "\(weather.temp) C")
            case 1:
                cell.configure(with: "Min Temperature", and: "\(weather.tempMin) C")
            case 2:
                cell.configure(with: "Max Temperature", and: "\(weather.tempMax) C")
            case 3:
                cell.configure(with: "Pressure", and: "\(weather.pressure) kPa")
            case 4:
                cell.configure(with: "Humidity", and: "\(weather.humidity) %")
            default:
                break
            }
        case 2:
            if indexPath.row == 0 {
                cell.configure(with: "Sunrise", and: weather.sunriseTime.time)
            } else {
                cell.configure(with: "SunSet", and: weather.sunsetTime.time)
            }
            
        case 3:
            cell.configure(with: "Speed", and: "\(weather.wind.speed) m/s")
        case 4:
            if indexPath.row == 0 {
                cell.configure(with: "Longitude", and: "\(weather.coordinates.lon)")
            } else {
                cell.configure(with: "Latitude", and: "\(weather.coordinates.lat)")
            }
        default:
            break
        }
        
        return cell
    }
}

extension CityDetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Weather"
        case 1:
            return "Temperature"
        case 2:
            return "Sun"
        case 3:
            return "Wind"
        case 4:
            return "Coordinates"
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
