//
//  Weather.swift
//  Weather
//
//  Created by Temirlan Merekeyev on 10/27/19.
//  Copyright © 2019 Merekeyev. All rights reserved.
//

import Foundation

struct Weather: Decodable {
    
    let status: String
    let temp: Double
    let pressure: Int
    let humidity: Int
    let tempMin: Double
    let tempMax: Double
    let sunriseTime: Int
    let sunsetTime: Int
    let wind: Wind
    let coordinates: Coordinates
    
    private enum CodingKeys: String, CodingKey {
        case weather
        case main
        case sys
        case coord
        case wind
    }
    
    private enum MainInfo: String, CodingKey {
        case temp
        case pressure
        case humidity
        case tempMin
        case tempMax
    }
    
    private enum Sys: String, CodingKey {
        case sunset
        case sunrise
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let mainInfoContainer = try container.nestedContainer(keyedBy: MainInfo.self, forKey: .main)
        
        temp = try mainInfoContainer.decode(Double.self, forKey: .temp)
        pressure = try mainInfoContainer.decode(Int.self, forKey: .pressure)
        humidity = try mainInfoContainer.decode(Int.self, forKey: .humidity)
        tempMin = try mainInfoContainer.decode(Double.self, forKey: .tempMin)
        tempMax = try mainInfoContainer.decode(Double.self, forKey: .tempMax)
        
        let sysContainer = try container.nestedContainer(keyedBy: Sys.self, forKey: .sys)
        
        sunsetTime = try sysContainer.decode(Int.self, forKey: .sunset)
        sunriseTime = try sysContainer.decode(Int.self, forKey: .sunrise)
        
        let statuses = try container.decode([Status].self, forKey: .weather)
        status = statuses.first?.description ?? ""
        wind = try container.decode(Wind.self, forKey: .wind)
        coordinates = try container.decode(Coordinates.self, forKey: .coord)
    }
}

struct Status: Decodable {
    
    let description: String
}

struct Wind: Decodable {
    
    let speed: Int
}

struct Coordinates: Decodable {
    
    let lat: Double
    let lon: Double
}
