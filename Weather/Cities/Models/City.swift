//
//  City.swift
//  Weather
//
//  Created by Temirlan Merekeyev on 10/27/19.
//  Copyright © 2019 Merekeyev. All rights reserved.
//

import Foundation

struct City {
    
    let name: String
    
    init(name: String) {
        self.name = name
    }
}
