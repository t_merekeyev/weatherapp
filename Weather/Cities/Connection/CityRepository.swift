//
//  CityRepository.swift
//  Weather
//
//  Created by Temirlan Merekeyev on 10/28/19.
//  Copyright © 2019 Merekeyev. All rights reserved.
//

import Moya

protocol CityRepository {
    
    typealias WeatherCompletion = (Weather) -> Void
    typealias FailureCompletion = (String) -> Void
    
    func getWeather(by cityName: String, success: @escaping WeatherCompletion, failure: @escaping FailureCompletion)
}

class CityAPIRepository: CityRepository {
    
    private let client = MoyaProvider<CityClient>()
    
    func getWeather(by cityName: String, success: @escaping WeatherCompletion, failure: @escaping FailureCompletion) {
        client.request(.getWeather(cityName: cityName)) { result in
            switch result {
            case .success(let response):
                if response.statusCode >= 200 && response.statusCode <= 300 {
                    do {
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        print(response.data)
                        let weather = try decoder.decode(Weather.self, from: response.data)
                        success(weather)
                    } catch {
                        failure("Decoding error")
                    }
                } else {
                    failure("Error. Response status code is \(response.statusCode)")
                }
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
    }
}
