//
//  CitiesClient.swift
//  Weather
//
//  Created by Temirlan Merekeyev on 10/28/19.
//  Copyright © 2019 Merekeyev. All rights reserved.
//

import Moya

enum CityClient: TargetType {
    
    case getWeather(cityName: String)
    
    var baseURL: URL {
        return URL(string: "https://api.openweathermap.org/data/2.5")! // I prefer using safe unwrapping, but in this case we cane use force unwrap cause we see crash when app will launch
    }
    
    var path: String {
        switch self {
        case .getWeather:
            return "/weather"
        }
    }
    
    var method: Method {
        switch self {
        case .getWeather:
            return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getWeather:
            return Data()
        }
    }
    
    var task: Task {
        switch self {
        case .getWeather(let name):
            return .requestParameters(parameters: ["q": name, "appid": "dbc5210da2808707fd73398701c162b0", "units": "metric"], encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
}
