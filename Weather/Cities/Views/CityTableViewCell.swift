 
 //
//  CityTableViewCell.swift
//  Weather
//
//  Created by Temirlan Merekeyev on 10/27/19.
//  Copyright © 2019 Merekeyev. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    
    func configure(with city: City) {
        nameLabel.text = city.name
    }
}
