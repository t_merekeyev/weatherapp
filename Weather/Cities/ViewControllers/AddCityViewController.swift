//
//  AddCityViewController.swift
//  Weather
//
//  Created by Temirlan Merekeyev on 10/28/19.
//  Copyright © 2019 Merekeyev. All rights reserved.
//

import UIKit

protocol AddCityViewControllerDelegate: class {
    
    func addCityViewControllerDidDismiss(_ viewController: AddCityViewController, cityName: String)
}

class AddCityViewController: UIViewController, CanDisplayAlerts {
    
    @IBOutlet private weak var textField: UITextField!
    
    weak var delegate: AddCityViewControllerDelegate?
    
    init() {
        super.init(nibName: AddCityViewController.identifier, bundle: nil)
        modalPresentationStyle = .overCurrentContext
        modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        textField.delegate = self
        textField.becomeFirstResponder()
    }
    
    @IBAction private func done(_ sender: UIButton) {
        delegate?.addCityViewControllerDidDismiss(self, cityName: textField.text ?? "")
        self.dismiss(animated: true)
    }
}

extension AddCityViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let name = textField.text {
            delegate?.addCityViewControllerDidDismiss(self, cityName: name)
            self.dismiss(animated: true)
            return true
        } else {
            self.dismiss(animated: true)
            return false
        }
    }
}
