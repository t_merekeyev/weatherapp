//
//  CitiesViewController.swift
//  Weather
//
//  Created by Temirlan Merekeyev on 10/27/19.
//  Copyright © 2019 Merekeyev. All rights reserved.
//

import UIKit

class CitiesViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    
    private var cities: [City]
    
    init(cities: [City]) {
        self.cities = cities
        super.init(nibName: CitiesViewController.identifier, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CityTableViewCell.nib, forCellReuseIdentifier: CityTableViewCell.identifier)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addCity))
        navigationItem.title = "Weather Application"
    }
    
    @objc
    private func addCity() {
        let vc = AddCityViewController()
        vc.delegate = self
        self.present(vc, animated: true)
    }
}

extension CitiesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CityTableViewCell.identifier, for: indexPath) as! CityTableViewCell
        cell.configure(with: cities[indexPath.row])
        return cell
    }
}

extension CitiesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 36.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let repository = CityAPIRepository()
        let vc = CityDetailsViewController(city: cities[indexPath.row], repository: repository)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension CitiesViewController: AddCityViewControllerDelegate {
    
    func addCityViewControllerDidDismiss(_ viewController: AddCityViewController, cityName: String) {
        if cityName.isEmpty {
            return
        }
        
        let city = City(name: cityName)
        cities.append(city)
        
        tableView.reloadData()
    }
}
